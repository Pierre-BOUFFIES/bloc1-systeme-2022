## Exercices avec le terminal Windows10
#### [Zoubeïda ABDELMOULA](mailto:zoubeida.abdelmoula@gmail.com), ESIEE-IT
#### Cours Bloc1 Système (BTS - SIO) - 09/11/2022

### Avant de commencer...

Toutes les commandes sont à consigner dans un même document :`B1-Systeme/ExercicesCMD-W10.md`. Chaque commande est précédée par le chemin du dossier courant et accompagnée d'une explication..

On utilisera l'utilitaire Invite de Commandes de Windows.

### Exercice 1

Construire avec les commandes Windows l’arborescence ci-contre dans un dossier `ligne-commande` créé au préalable dans votre dossier personnel:

- Le fichier `fichier11.txt` doit contenir la ligne “MS-DOS”.
- Le fichier `fichier12.txt` doit contenir la ligne “Windows”.
- Le fichier `fichier22.txt` doit contenir la ligne “Linux”.
 

![arborescence](img/cmd_win_1.png)

### Exercice 2

Trouvez comment faire pour :

- Vous déplacer dans la partition `M:` (ou tout autre partition de votre système de fichier)
- Afficher le contenu de cette partition.
- Créer un dossier exo2
- Vous y déplacer.
- Créer un fichier vide moi.txt dans le dossier courant.
- Afficher le message “Je suis en BTS SIO”.
- Ecrire le message précédent dans le fichier moi.txt.
- Ajouter au fichier moi.txt la ligne “Et j’aime ça !”.
- Vous déplacer à la racine de la partition.


### Exercice 3

On suppose que le dossier courant est votre dossier personnel.
En une seule commande et sans utiliser d’éditeur de texte, trouvez comment faire pour :

- Créer un dossier `exo3`
- Vous y déplacer.
- Créer un fichier vide `moi.txt` dans le dossier courant.
- Afficher le message “Je suis en BTS SIO”.
- Ecrire le message précédent dans le fichier `moi.txt`.
- Lister le contenu (avec détail) du dossier courant. 
- Ajouter au fichier `moi.txt` le contenu proposé par le listing précédent.


En utilisant un éditeur de texte, créez le fichier `presidents.txt` ayant le contenu suivant :
```
François MITTERRAND
Jacques CHIRAC
Georges POMPIDOU
```

Créez aussi le fichier `prix.txt` ayant le contenu suivant :
`L'inflation commence à toucher plusieurs postes budgétaires.`

En une seule commande, trouvez ensuite comment faire pour :

- Dupliquer `president.txt` sous le nom `presidentsFrance.txt`.
- Déplacer tous les fichiers sauf `prix.txt` dans votre dossier personnel.
- Revenir dans votre dossier personnel.
- Supprimer les fichiers créés précédemment.
- Supprimer le dossier `exo3`.
- Réinitialiser le contenu de la console.

### Exercice 4

Trouvez comment faire pour :

- Créer le dossier `bin` dans votre dossier personnel.
- Afficher la liste des variables d’environnement.
- Afficher le nom de l’utilisateur courant.

Modifiez votre environnement pour :
- Ajouter le dossier `bin` créé précédemment à la variable PATH.
- Créer l’alias `dd` qui lance la commande `dir /p`.

Testez vos modifications.

